$(document).ready(function () 
{
  $(".tombolTambahData").on("click", function () {
    $("#formModalLabel").html("Tambah Data Blog");
    $(".modal-footer button[type=submit]").html("Tambah Data");
  });

  $(".tombolTambahUser").on("click", function () {
    $("#judulModalLabel").html("Tambah Data User");
    $(".modal-footer button[type=submit]").html("Tambah Data");
  });

  $(".tampilModalEditUser").on("click", function () {
    $("#judulModalLabel").html("Ubah Data User");
    $(".modal-footer button[type=submit]").html("Ubah Data");
    $(".modal-body form").attr(
      "action",
      "http://localhost/phpmvc/public/user/ubah"
    );

    const id = $(this).data("id");

    $.ajax({
      url: "http://localhost/phpmvc/public/user/getubah",
      data: { id: id },
      method: "post",
      dataType: "json",
      success: function (data) {
        $("#username").val(data.username);
        $("#email").val(data.email);
        $("#password").val(data.password);
        $("#id").val(data.id);
      },
    });
  });

  $(".tampilModalEdit").on("click", function () {
    $("#formModalLabel").html("Ubah Data Blog");
    $(".modal-footer button[type=submit]").html("Ubah Data");
    $(".modal-body form").attr(
      "action",
      "http://localhost/phpmvc/public/blog/ubah"
    );

    const id = $(this).data("id");

    $.ajax({
      url: "http://localhost/phpmvc/public/blog/getubah",
      data: { id: id },
      method: "post",
      dataType: "json",
      success: function (data) {
        $("#penulis").val(data.penulis);
        $("#judul").val(data.judul);
        $("#tulisan").val(data.tulisan);
        $("#id").val(data.id);
      },
    });
  });

  $(".tampilModalTambah").on("click", function () {
    $(".modal-body form").attr(
      "action",
      "http://localhost/phpmvc/public/blog/tambah"
    );
    $("#formModalLabel").html("Tambah Data Blog");
    $(".modal-footer button[type=submit]").html("Tambah Data");
    $("#penulis").val("");
    $("#judul").val("");
    $("#tulisan").val("");
    $("#id").val("");
  });

  $(".tampilModalUser").on("click", function () {
    $(".modal-body form").attr(
      "action",
      "http://localhost/phpmvc/public/user/tambah"
    );
    $("#formModalLabel").html("Tambah Data User");
    $(".modal-footer button[type=submit]").html("Tambah Data");
    $("#username").val("");
    $("#email").val("");
    $("#password").val("");
    $("#id").val("");
  });
});
