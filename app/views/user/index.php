<div class="container mt-3 justify-content-center">
  <div class="row">
    <div class="col-lg-6">
      <?php Flasher::flash(); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-6">
      <button type="button" class="btn btn-primary tombolTambahUser" data-bs-toggle="modal" data-bs-target="#formModal">
        Tambah Daftar User
      </button>
      </br>
      <h3>Daftar User</h3>
      <ul class="list-group">
        <?php foreach ($data["user"] as $user) : ?>
          <li class="list-group-item list-group-item">
            <?= $user['username']; ?>
            <a href="<?= BASE_URL; ?>/user/hapus/<?= $user['id']; ?>" class="badge bg-danger text-decoration-none float-end ms-1" onclick="return confirm ('yakin?');">Hapus</a>
            <a href="<?= BASE_URL; ?>/user/ubah/" class="badge bg-success text-decoration-none float-end ms-1 tampilModalEditUser" data-bs-toggle="modal" data-bs-target="#formModal" data-id="<?= $user['id']; ?>">Ubah</a>
            <a href="<?= BASE_URL; ?>/user/profile/<?= $user['id']; ?>" class="badge bg-primary text-decoration-none float-end ms-1">Detail</a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModalLabel">Tambah Data User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= BASE_URL; ?>/user/tambah" method="post">
          <input type="hidden" name="id" id="id">
          <div class="mb-3">
            <label for="username" class="form-label">Username</label>
            <input type="text" class="form-control" id="username" name="username">
          </div>
          <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email">
          </div>
          <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" name="password">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
      </div>
    </div>
  </div>
</div>