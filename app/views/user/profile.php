<div class="container text-center mt-4">
 <h1>Halaman User</h1>
 <img src="<?= BASE_URL; ?>/img/profile-user.png" class="rounded-circle shadow" width="200px">
 <p>Halo, nama saya <?= $data["user"]["username"]; ?>, dengan alamat email <?= $data["user"]["email"];?></p>
 <a href="<?= BASE_URL; ?>/user/" class="card-link">Back</a>
</div>
