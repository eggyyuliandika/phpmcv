<div class="container mt-4"> 
    <div class="row justify-content-center"> 
        <div class="col-6 shadow rounded-4 p-4"> 
            <h1 class="text-center">Halaman Register</h1> 
            <form method="POST" action="<?= BASE_URL; ?>/register/regisProses"> 
                <?= Flasher::Flash(); ?> 
                <div class="mb-3"> 
                    <label for="penulis" class="form-label">Username</label> 
                    <input type="text" class="form-control" id="penulis" name="username"> 
                </div> 
                <div class="mb-3"> 
                    <label for="email" class="form-label">Email</label> 
                    <input type="email" class="form-control" id="email" name="email"> 
                </div> 
                <div class="row"> 
                    <div class="col"> 
                        <div class="mb-3"> 
                            <label for="password" class="form-label">Password</label> 
                            <input type="password" class="form-control" id="password" name="password"> 
                        </div> 
                        <div class="mb-3"> 
                            <label for="confirm_password" class="form-label">Confirm Password</label> 
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password"> 
                        </div> 
                    </div> 
                </div> 
                <button type="submit" class="btn btn-primary">Submit</button> 
            </form> 
        </div> 
    </div> 
</div>