<?php
 class User_model {
    private $table = 'user';
    private $db;
  
    public function __construct() {
        $this->db = new Database;
    }
  
    public function getAllUser() {
       $this->db->query("SELECT * FROM {$this->table}");
       return $this->db->resultAll();
    }
    public function getUserById($id) {
      $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
      $this->db->bind("id",$id);
      return $this->db->resultSingle();
    }

    public function getUserByEmail($email)
    {
      $query = ("SELECT * FROM $this->table WHERE email = :email");
      $this->db->query($query);
      $this->db->bind("email", $email);
      return $this->db->resultSingle();
    }
  
    public function tambahDataUser($data)
    {

      $password = md5($data['password']) . SALT; 

      $query = "INSERT INTO user
                    VALUES
                (NULL, :username, :email, :password)";
  
      $this->db->query($query);
      $this->db->bind('username', $data['username']);
      $this->db->bind('email', $data['email']);
      $this->db->bind('password', $password);
  
      $this->db->execute();
  
      return $this->db->rowCount();
    }

   
    public function hapusDataUser($id)
    {
      $query = "DELETE FROM user WHERE id = :id";
      $this->db->query($query);
      $this->db->bind('id', $id);
  
      $this->db->execute();
  
      return $this->db->rowCount();
    }

   
  
  }
  ?>

     
 