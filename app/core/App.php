<?php
  class App {
    //beri nilai default untuk mengantisipasi bila url yang diberikan tidak ditemukan controller atau method
    protected $controller = 'Home';
    protected $method = 'index';
    protected $params = [];
   
    public function __construct(){
      $url = $this->parseURL();

      $controller = $url[0] ?? $this->controller;
   
      //setup controller
      //file_exists() mengecek apakah controller yang diminta user ada atau tidak filenya di app kita, kalau ada simpan nilainya pada variable $this->controller
      // unset() menghapus elemen controler pada array
      if(file_exists("../app/controllers/". $controller.'.php')) {
        //menimpa controller diatas, menjadi controller baru dan diisi dengan url ke 0
        $this->controller = $controller;
        unset($url[0]);
      }
      //memanggil controller, menggabungkan dgn controller baru, lalu instansiasi controller, agar nanti bisa mengambil methodnya
      require_once "../app/controllers/". $this->controller.'.php';
      $this->controller = new $this->controller;

      //setup method
      // method_exists() mengecek apakah url itu memiliki method sesuai dengan data yang kita dapat dari url
      // jika ada set nilainya dan juga hapus elemen menggunakan unset()
      if(isset($url[1])) {
        if(method_exists($this->controller, $url[1])) {
          $this->method= $url[1];
          unset($url[1]);
        }
      }

      //setup params/data
      // jika url mengandung data 
      // nah jika mengandung data maka data akan dimasukkan ke variabel params menggunakan method array_values()
      if(!empty($url)) {
        $this->params = array_values($url);
      }

      //jalankan controller dan method serta kirim params jika ada
      call_user_func_array([$this->controller, $this->method], $this->params);
    }


    public function parseURL() {
      //jika ada url yang dikirimkan, maka ambil isi urlnya
      if(isset($_GET['url'])) {
        $url = rtrim($_GET['url'], '/' );
        // untuk menghilangkan karaktek yg tidak diinginkan, seperti "/"

        $url = filter_var($url, FILTER_SANITIZE_URL); 
        // untuk memfilter dari karakter asing yang memungkinkan website dimanupulasi

        $url = explode('/', $url);
        // untuk memecah string yang kita punya menjadi bagian-bagian dalam bentuk array, menjadi controller, method, dan data

        return $url;
        
      }
    }
  }

  // htaccess untuk memodifikasi konfigurasi dari server apache yang bisa dilakukan pada setiap folder
  // seperti memblok akses
  